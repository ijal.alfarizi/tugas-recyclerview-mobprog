package com.rizalalfarizi1600807.asus.tugasmovielist;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.rizalstudio.asus.tugasmovielist.R;

import java.util.ArrayList;

import adapter.CardViewAdapter;
import model.Movie;
import model.MovieData;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvMovie;
    private ArrayList<Movie> movies;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("MOVIE LIST");
        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#cc0000")));

        rvMovie = (RecyclerView) findViewById(R.id.rv_movie);
        rvMovie.setHasFixedSize(true);
        MovieData mvData = new MovieData();

        movies = new ArrayList<>();
        movies.addAll(mvData.getMovies());

        showMovie();

    }

    private void showMovie(){
        rvMovie.setLayoutManager(new LinearLayoutManager(this));
        CardViewAdapter cardViewAdapter = new CardViewAdapter(this);
        cardViewAdapter.setMovies(movies);
        rvMovie.setAdapter(cardViewAdapter);
    }
}
