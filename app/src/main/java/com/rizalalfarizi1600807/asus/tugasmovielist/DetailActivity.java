package com.rizalalfarizi1600807.asus.tugasmovielist;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rizalstudio.asus.tugasmovielist.R;

import model.Movie;

public class DetailActivity extends AppCompatActivity {
    public static String EXTRA_MOVIE = "extra_movie";
    private Movie data;
    TextView tvTitle,tvYear,tvCategory,tvGenre,tvReleaseDate,tvDuration,tvDirector,tvWriter,tvStarring,tvSynopsis;
    ImageView ivPoster;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#cc0000")));

        data = getIntent().getExtras().getParcelable(EXTRA_MOVIE);
        getSupportActionBar().setTitle("Movie: "+data.getTitle());
        tvTitle = (TextView) findViewById(R.id.title);
        tvCategory = (TextView) findViewById(R.id.category);
        tvYear= (TextView) findViewById(R.id.release_year);
        tvGenre = (TextView) findViewById(R.id.genre);
        tvReleaseDate = (TextView) findViewById(R.id.release_date);
        tvDuration = (TextView) findViewById(R.id.duration);
        tvDirector = (TextView) findViewById(R.id.director);
        tvWriter = (TextView) findViewById(R.id.writer);
        tvStarring = (TextView) findViewById(R.id.starring);
        tvSynopsis = (TextView) findViewById(R.id.synopsis);
        ivPoster = (ImageView) findViewById(R.id.poster);

        Glide.with(this)
                .load(data.getPoster())
                .into(ivPoster);

        tvTitle.setText(data.getTitle());
        tvSynopsis.setText(data.getSynopsis());
        tvStarring.setText(data.getStarring());
        tvYear.setText(data.getRelease_year());
        tvReleaseDate.setText(data.getRelease_date());
        tvGenre.setText(data.getGenre());
        tvDirector.setText(data.getDirector());
        tvWriter.setText(data.getWriter());
        tvCategory.setText(data.getCategory());
        tvDuration.setText(data.getDuration());


    }


}
