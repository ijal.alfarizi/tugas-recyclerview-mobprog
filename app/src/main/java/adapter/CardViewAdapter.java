package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rizalalfarizi1600807.asus.tugasmovielist.DetailActivity;
import com.rizalstudio.asus.tugasmovielist.R;

import java.util.ArrayList;

import helper.CustomOnItemClickListener;
import model.Movie;

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.CVViewHolder> {
    private ArrayList<Movie> movies;
    private Context context;

    public CardViewAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public CardViewAdapter.CVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new CVViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewAdapter.CVViewHolder holder, int position) {
        Movie data = movies.get(position);

        Glide.with(context)
                .load(data.getPoster())
                .override(150,250)
                .into(holder.poster);
        holder.title.setText(data.getTitle()+" ("+ data.getRelease_year()+")");
        holder.starring.setText(data.getStarring());
        holder.director.setText(data.getDirector());
        holder.rating.setText(""+data.getRating());
        holder.btn_detail.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                Movie data = new Movie();
                data = getMovies().get(position);

                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.EXTRA_MOVIE, data);
                context.startActivity(detailIntent);
//                Toast.makeText(context,"AAA",Toast.LENGTH_SHORT).show();
            }
        }));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    class CVViewHolder extends RecyclerView.ViewHolder{
        ImageView poster;
        TextView title, director,starring,rating;
        Button btn_detail;


        public CVViewHolder(View itemView) {
            super(itemView);
            poster = (ImageView) itemView.findViewById(R.id.poster);
            title = (TextView) itemView.findViewById(R.id.title);
            director = (TextView) itemView.findViewById(R.id.director);
            starring = (TextView) itemView.findViewById(R.id.starring);
            rating = (TextView) itemView.findViewById(R.id.rating);
            btn_detail = (Button) itemView.findViewById(R.id.btn_detail);

        }
    }
}
