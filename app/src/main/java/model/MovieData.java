package model;

import java.util.ArrayList;

public class MovieData {
    private static ArrayList<Movie> movies;


    public MovieData() {
        movies = new ArrayList<>();
        Movie data = new Movie();
        data.setTitle("Cidade de Deus");
        data.setCategory("R");
        data.setDirector("Fernando Meirelles");
        data.setDuration("2h 10min ");
        data.setGenre("Crime, Drama");
        data.setRating(8.6);
        data.setRelease_date("30 August 2002 (Brazil)");
        data.setRelease_year("2002");
        data.setStarring("Alexandre Rodrigues, Leandro Firmino, Matheus Nachtergaele");
        data.setSynopsis("Brazil, 1960s, City of God. The Tender Trio robs motels and gas trucks. Younger kids watch and learn well...too well. 1970s: Li'l Zé has prospered very well and owns the city. He causes violence and fear as he wipes out rival gangs without mercy. His best friend Bené is the only one to keep him on the good side of sanity. Rocket has watched these two gain power for years, and he wants no part of it. Yet he keeps getting swept up in the madness. All he wants to do is take pictures. 1980s: Things are out of control between the last two remaining gangs...will it ever end? Welcome to the City of God.");
        data.setWriter("Paulo Lins (novel), Bráulio Mantovani (screenplay)");
        data.setPoster("https://m.media-amazon.com/images/M/MV5BMGU5OWEwZDItNmNkMC00NzZmLTk1YTctNzVhZTJjM2NlZTVmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,677,1000_AL_.jpg");
        movies.add(data);

        data = new Movie();
        data.setTitle("Star Wars");
        data.setCategory("PG");
        data.setDirector("George Lucas");
        data.setDuration("2h 1min ");
        data.setGenre("Action , Adventure , Fantasy");
        data.setRating(8.6);
        data.setRelease_date("25 May 1977 (USA)");
        data.setRelease_year("1977");
        data.setStarring("Mark Hamill, Harrison Ford, Carrie Fisher");
        data.setSynopsis("The Imperial Forces, under orders from cruel Darth Vader, hold Princess Leia hostage in their efforts to quell the rebellion against the Galactic Empire. Luke Skywalker and Han Solo, captain of the Millennium Falcon, work together with the companionable droid duo R2-D2 and C-3PO to rescue the beautiful princess, help the Rebel Alliance and restore freedom and justice to the Galaxy.");
        data.setWriter("George Lucas");
        data.setPoster("https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,643,1000_AL_.jpg");
        movies.add(data);

        data = new Movie();
        data.setTitle("Blade Runner 2049");
        data.setCategory("17+");
        data.setDirector("Denis Villeneuve");
        data.setDuration("2h 44min ");
        data.setGenre("Drama , Mystery , Sci-Fi");
        data.setRating(8.0);
        data.setRelease_date("6 October 2017 (Indonesia)");
        data.setRelease_year("2017");
        data.setStarring("Harrison Ford, Ryan Gosling, Ana de Armas");
        data.setSynopsis("Thirty years after the events of the first film, a new blade runner, LAPD Officer K (Ryan Gosling), unearths a long-buried secret that has the potential to plunge what's left of society into chaos. K's discovery leads him on a quest to find Rick Deckard (Harrison Ford), a former LAPD blade runner who has been missing for 30 years.");
        data.setWriter("Hampton Fancher (screenplay by), Michael Green (screenplay by)");
        data.setPoster("https://m.media-amazon.com/images/M/MV5BNzA1Njg4NzYxOV5BMl5BanBnXkFtZTgwODk5NjU3MzI@._V1_SY1000_CR0,0,674,1000_AL_.jpg");
        movies.add(data);

        data = new Movie();
        data.setTitle("It's a Wonderful Life");
        data.setCategory("PG");
        data.setDirector("Frank Capra");
        data.setDuration("2h 10min ");
        data.setGenre("Drama , Family , Fantasy");
        data.setRating(8.6);
        data.setRelease_date("7 January 1947 (USA)");
        data.setRelease_year("1946");
        data.setStarring("James Stewart, Donna Reed, Lionel Barrymore ");
        data.setSynopsis("George Bailey has spent his entire life giving of himself to the people of Bedford Falls. He has always longed to travel but never had the opportunity in order to prevent rich skinflint Mr. Potter from taking over the entire town. All that prevents him from doing so is George's modest building and loan company, which was founded by his generous father. But on Christmas Eve, George's Uncle Billy loses the business's $8,000 while intending to deposit it in the bank. Potter finds the misplaced money and hides it from Billy. When the bank examiner discovers the shortage later that night, George realizes that he will be held responsible and sent to jail and the company will collapse, finally allowing Potter to take over the town. ");
        data.setWriter("Frances Goodrich (screenplay), Albert Hackett (screenplay)");
        data.setPoster("https://m.media-amazon.com/images/M/MV5BZjc4NDZhZWMtNGEzYS00ZWU2LThlM2ItNTA0YzQ0OTExMTE2XkEyXkFqcGdeQXVyNjUwMzI2NzU@._V1_SY1000_CR0,0,687,1000_AL_.jpg");
        movies.add(data);


    }

    public static ArrayList<Movie> getMovies() {
        return movies;
    }
}
