package model;

import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {
    private String title;
    private String category;
    private String genre;
    private String duration;
    private String release_date;
    private String release_year;
    private double rating;
    private String director;
    private String writer;
    private String starring;
    private String synopsis;
    private String poster;

    public Movie() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getRelease_year() {
        return release_year;
    }

    public void setRelease_year(String release_year) {
        this.release_year = release_year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getStarring() {
        return starring;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.category);
        dest.writeString(this.genre);
        dest.writeString(this.duration);
        dest.writeString(this.release_date);
        dest.writeString(this.release_year);
        dest.writeDouble(this.rating);
        dest.writeString(this.director);
        dest.writeString(this.writer);
        dest.writeString(this.starring);
        dest.writeString(this.synopsis);
        dest.writeString(this.poster);
    }

    protected Movie(Parcel in) {
        this.title = in.readString();
        this.category = in.readString();
        this.genre = in.readString();
        this.duration = in.readString();
        this.release_date = in.readString();
        this.release_year = in.readString();
        this.rating = in.readDouble();
        this.director = in.readString();
        this.writer = in.readString();
        this.starring = in.readString();
        this.synopsis = in.readString();
        this.poster = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
